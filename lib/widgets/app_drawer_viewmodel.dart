import 'package:flutter/cupertino.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/store/counter_state/counter_selectors.dart';
import 'package:redux/redux.dart';

class AppDrawerViewmodel {
  final void Function() incrementValue;
  final void Function() decrementValue;

  AppDrawerViewmodel({
    @required this.incrementValue,
    @required this.decrementValue,
  });

  static AppDrawerViewmodel fromStore (Store<AppState> store) {
    return AppDrawerViewmodel(
      incrementValue: CounterSelectors.getIncrementFunc(store),
      decrementValue: CounterSelectors.getDecrementFunc(store),
    );
  }
}
