import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutterreduxpr6/helpers/route_helper.dart';
import 'package:flutterreduxpr6/res/consts.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';

import 'app_drawer_viewmodel.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppDrawerViewmodel>(
      converter: AppDrawerViewmodel.fromStore,
      builder: (BuildContext context, AppDrawerViewmodel viewModel) {
        return Drawer(
          child: Column(
            children: <Widget>[
              RaisedButton(
                child: Text(INCREMENT),
                onPressed: viewModel.incrementValue,
              ),
              RaisedButton(
                child: Text(DECREMENT),
                onPressed: viewModel.decrementValue,
              ),
              const SizedBox(
                height: 20.0,
              ),
              RaisedButton(
                child: Text(PAGE_ONE_TITLE),
                onPressed: () {
                  Navigator.of(context).push(
                    RouteHelper.instance.onGenerateRoute(
                      RouteSettings(name: ROUTE_PAGE_ONE),
                    ),
                  );
                },
              ),
              RaisedButton(
                child: Text(PAGE_TWO_TITLE),
                onPressed: () {
                  Navigator.of(context).push(
                    RouteHelper.instance.onGenerateRoute(
                      RouteSettings(name: ROUTE_PAGE_TWO),
                    ),
                  );
                },
              ),
              RaisedButton(
                child: Text(PAGE_THREE_TITLE),
                onPressed: () {
                  Navigator.of(context).push(
                    RouteHelper.instance.onGenerateRoute(
                      RouteSettings(name: ROUTE_PAGE_THREE),
                    ),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
