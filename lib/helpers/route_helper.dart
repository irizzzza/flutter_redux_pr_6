import 'package:flutter/material.dart';
import 'package:flutterreduxpr6/pages/page_pne/page_one.dart';
import 'package:flutterreduxpr6/pages/page_three/page_three.dart';
import 'package:flutterreduxpr6/pages/page_two/page_two.dart';
import 'package:flutterreduxpr6/res/consts.dart';

class RouteHelper {
  // region [Initialization]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // endregion

  Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_PAGE_ONE:
        return _defaultRoute(
          settings: settings,
          page: PageOne(),
        );

      case ROUTE_PAGE_TWO:
        return _defaultRoute(
          settings: settings,
          page: PageTwo(),
        );

      case ROUTE_PAGE_THREE:
        return _defaultRoute(
          settings: settings,
          page: PageThree(),
        );

      default:
        return _defaultRoute(
          settings: settings,
          page: PageOne(),
        );
    }
  }

  MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    print(page);
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
