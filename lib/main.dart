import 'package:flutter/material.dart';
import 'package:flutterreduxpr6/app/application.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:redux/redux.dart';

void main() {
  Store store = Store<AppState>(
    AppState.reducer,
    initialState: AppState.initial(),
  );
  runApp(
    Application(
      store: store,
    ),
  );
}
