import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpr6/pages/page_pne/page_one_viewmodel.dart';
import 'package:flutterreduxpr6/res/consts.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/widgets/app_drawer.dart';

class PageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PageOneViewmodel>(
      converter: PageOneViewmodel.fromStore,
      builder: (BuildContext context, PageOneViewmodel viewModel) {
        return Scaffold(
          backgroundColor: Colors.amber,
          appBar: AppBar(
            title: Text(PAGE_ONE_TITLE),
          ),
          drawer: AppDrawer(),
          body: Center(
            child: Text('${viewModel.getCounterValue}'),
          ),
        );
      }

    );
  }
}
