import 'package:flutter/foundation.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/store/counter_state/counter_selectors.dart';
import 'package:redux/redux.dart';

class PageOneViewmodel {
  final int getCounterValue;

  PageOneViewmodel({
    @required this.getCounterValue,
  });

  static PageOneViewmodel fromStore (Store<AppState> store) {
    return PageOneViewmodel(
      getCounterValue: CounterSelectors.getCounterValue(store),
    );
  }
}