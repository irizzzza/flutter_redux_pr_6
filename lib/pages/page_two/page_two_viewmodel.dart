import 'package:flutter/foundation.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/store/counter_state/counter_selectors.dart';
import 'package:redux/redux.dart';

class PageTwoViewmodel {
  final int getCounterValue;

  PageTwoViewmodel({
    @required this.getCounterValue,
  });

  static PageTwoViewmodel fromStore (Store<AppState> store) {
    return PageTwoViewmodel(
      getCounterValue: CounterSelectors.getCounterValue(store),
    );
  }
}