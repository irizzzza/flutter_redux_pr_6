import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpr6/pages/page_two/page_two_viewmodel.dart';
import 'package:flutterreduxpr6/res/consts.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';

class PageTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PageTwoViewmodel>(
      converter: PageTwoViewmodel.fromStore,
      builder: (BuildContext ctx, PageTwoViewmodel viewmodel){
        return  Scaffold(
          backgroundColor: Colors.blueAccent,
          appBar: AppBar(
            title: Text(PAGE_TWO_TITLE),
          ),
          body: Center(
            child: Text('${viewmodel.getCounterValue / 2}'),
          ),
        );
      },
    );
  }
}
