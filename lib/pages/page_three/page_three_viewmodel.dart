import 'package:flutter/cupertino.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/store/counter_state/counter_selectors.dart';
import 'package:redux/redux.dart';

class PageThreeViewmodel {
    final int getCounterValue;

    PageThreeViewmodel({
      @required this.getCounterValue,
});

    static PageThreeViewmodel fromStore (Store<AppState> store) {
      return PageThreeViewmodel(
        getCounterValue: CounterSelectors.getCounterValue(store),
      );
    }
}