import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpr6/pages/page_three/page_three_viewmodel.dart';
import 'package:flutterreduxpr6/res/consts.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';

class PageThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PageThreeViewmodel>(
      converter: PageThreeViewmodel.fromStore,
      builder: (BuildContext context, PageThreeViewmodel viewModel) {
        return Scaffold(
          backgroundColor: Colors.lime,
          appBar: AppBar(
            title: Text(PAGE_THREE_TITLE),
          ),
          body: Center(
            child: Text('${viewModel.getCounterValue * 10}'),
          ),
        );
      },
    );
  }
}
