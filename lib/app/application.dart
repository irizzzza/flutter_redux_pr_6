import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpr6/pages/page_pne/page_one.dart';
import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:redux/redux.dart';

class Application extends StatelessWidget {
  final Store<AppState> store;

  Application({
    @required this.store,
  });

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: PageOne(),
      ),
    );
  }
}
