import 'package:flutterreduxpr6/store/counter_state/counter_state.dart';

class AppState {

  final CounterState counterState;

  AppState({this.counterState});

  factory AppState.initial() => AppState(
    counterState: CounterState.initial(),
  );

  static AppState reducer (AppState state, dynamic action) {
    return AppState(
      counterState: state.counterState.reducer(action),
    );
  }
}