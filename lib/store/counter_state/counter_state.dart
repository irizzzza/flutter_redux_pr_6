import 'dart:collection';

import 'package:flutterreduxpr6/store/counter_state/actions/counter_decrement_action.dart';
import 'package:flutterreduxpr6/store/counter_state/actions/counter_increment_action.dart';
import 'package:flutterreduxpr6/store/reducer.dart';

class CounterState {
  static const String TAG = 'CounterState';
  final int counterValue;

  CounterState({this.counterValue});

  factory CounterState.initial() => CounterState(counterValue: 0,);


  CounterState copyWith({

    int counter
  }) {
    return CounterState(
      counterValue: counter ?? this.counterValue,
    );
  }

  CounterState reducer(dynamic action) {
    print('$TAG => Action runtimetype => ${action.runtimeType}');

    return Reducer<CounterState>(
      actions: HashMap.from({
        CounterIncrementAction: (dynamic action) =>
            incrementCounter(),
        CounterDecrementAction: (dynamic action) =>
            decrementCounter(),
      }),

    ).updateState(action, this);
  }

  CounterState incrementCounter() {
    return this.copyWith(
      counter: this.counterValue + 1,
    );
  }

  CounterState decrementCounter() {
    return this.copyWith(
      counter: this.counterValue - 1,
    );
  }
}