import 'package:flutterreduxpr6/store/appstate/appstate.dart';
import 'package:flutterreduxpr6/store/counter_state/actions/counter_decrement_action.dart';
import 'package:flutterreduxpr6/store/counter_state/actions/counter_increment_action.dart';
import 'package:redux/redux.dart';

class CounterSelectors {
   static void Function() getIncrementFunc(Store<AppState> store) {
     return () => store.dispatch(CounterIncrementAction());
   }

   static void Function() getDecrementFunc(Store<AppState> store) {
     return () => store.dispatch(CounterDecrementAction());
   }

   static int getCounterValue(Store<AppState> store) {
     return store.state.counterState.counterValue;
   }
}