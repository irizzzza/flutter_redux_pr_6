
const String INCREMENT = 'Increment';
const String DECREMENT = 'Decrement';


const String PAGE_ONE_TITLE = 'Page one';
const String PAGE_TWO_TITLE = 'Page two';
const String PAGE_THREE_TITLE = 'Page three';


const String ROUTE_PAGE_ONE = '/page_one';
const String ROUTE_PAGE_TWO = '/page_two';
const String ROUTE_PAGE_THREE = '/page_three';
